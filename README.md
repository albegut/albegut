# Práctica 4 #
## Tecnologías para el desarrollo de software ##
### Alberto Gutiérrez Pérez ###

Repositorio de la práctica final opcional mediante la que se pretende realizar un ciclo completo Rojo-Verde-Refactor. 

##Tecnologías utilizadas
| Tecnología | Versión utilizada |  
|-----------:|:------------------|  
|Java        | 1.7               |  
|Eclipse     | Luna release      |  
|Ant         | 1.9.4             |  
|Maven       | 3.3.9             |  
|JUnit       | 4                 |  

La realización se ha llevado a cabo en tres etapas tal y como se estipula en el enunciado de la práctica. 
Cada una de las etapas cuenta con una rama dentro del repositiorio de la siguiente forma:  
   Etapa --> Rama  
1. Desarrollo basado en tdd --> tdd  
2. Implementación --> implementation  
3. Refactoring --> refactor  

