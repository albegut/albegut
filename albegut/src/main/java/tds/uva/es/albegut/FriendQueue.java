package tds.uva.es.albegut;

import java.util.LinkedList;

public class FriendQueue {

	/**
	 * List of the FriendQueue
	 */
	private LinkedList<Person> queue;
	
	/**
	 * Constructs a FriendQueue with an empty list 
	 */
	public FriendQueue(){
		queue = new LinkedList<Person>();
	}
	
	
	/**
	 * Retrieves, but does not remove, the first {@link Person} to be attended  
	 * @return The {@link Person} to be attended 
	 * @throws IllegalStateException if there is not a {@link Person} to be attended
	 */
	public Person nextPersonToAttend() throws IllegalStateException {
		if (queue.size()==0) throw new IllegalStateException("There is not a Person to be attended");
		return queue.peek();
	}
	
	/**
	 * Appends the given {@link Person} to the end of the {@link FriendQueue}
	 * @param person {@link Person} to be appended 
	 * @throws IllegalArgumentException if the {@link Person} has already appended or is null
	 */
	public void addPerson(Person person) throws IllegalArgumentException {
		if(person==null) throw new IllegalArgumentException("Invalid value, it needs a reference to a Person");
		if(queue.contains(person))throw new IllegalArgumentException("The given person is already in this FriendQueue");
		queue.add(person);
		
	}
	
	/**
	 * Appends the given {@link Person} at the specified position in the {@link FriendQueue}
	 * @param person {@link Person} to be appended 
	 * @param position which the specified person is to be appended
	 * @throws IllegalArgumentException if the {@link Person} has already appended or is null
	 */
	public void addPerson(Person person, int position) throws IllegalArgumentException {
		queue.add(position-1,person);
		
	}
	
	
	/**
	 * Retrieves and removes the first {@link Person} to be attended
	 * @return The next {@link Person} to be attended 
	 * @throws IllegalStateException if there is not a {@link Person} to be attended
	 */
	public Person attendNextPerson() throws IllegalStateException{
		if (queue.size()==0) throw new IllegalStateException("There is not a Person to be attended");
		return queue.poll();
	}
	
	/**
	 * Looks up on the {@link FriendQueue} for the given {@link Person}.
	 * @param person {@link Person} to be looked up
	 * @return true if this {@link FriendQueue} contains the given {@link Person}, false otherwise
	 */
	public boolean contains(Person person){
		return queue.contains(person);
	}

	/**
	 * Looks up the position of the given {@link Person}
	 * @param  person {@link Person} 
	 * @return the position of the given {@link Person}
	 * @throws IllegalArgumentException if there is not a {@link Person} in the {@link FriendQueue}
	 * or the given {@link Person} is null.
	 */
	public int getPosition(Person person) throws IllegalArgumentException{
		if(person==null) throw new IllegalArgumentException("Invalid value, it needs a reference to a Person");
		if(!queue.contains(person)) throw new IllegalArgumentException("The given person is not in this FriendQueue");
		return queue.indexOf(person)+1;
	}

	/**
	 * Returns the {@link Person} at the specified position
	 * @param position of the person to return
	 * @return the person at the specified position
	 */
	public Person getPerson(int position) {
		return queue.get(position-1);
	}

}
