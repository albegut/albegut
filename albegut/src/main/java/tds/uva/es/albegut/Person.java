package tds.uva.es.albegut;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class Person {
	
	
	/**
	 * Maximum number of friends to sneak
	 */
	private static final int MAX = 10;

	/**
	 * Minimum number of friends to sneak
	 */
	private static final int MIN = 0;

	/**
	 * Name of the person
	 */
	private String name;
	
	/**
	 * Person friends
	 */
	private ArrayList<Person> friends;
	
	/**
	 * Person acquaintances
	 */
	private ArrayList<Person> acquaintances;
	
	/**
	 * Total number of friends who is going to come with the person
	 */
	private int totalFriendsToSneak;
	
	/**
	 * Number of friends which the person has already sneaked 
	 */
	private int friendsSneaked;

	/**
	 * Constructs a {@link Person} with the given parameters
	 * @param name of the {@link Person}
	 * @param friends of the {@link Person}
	 * @param acquaintances of the {@link Person}
	 * @throws IllegalArgumentException if either the name is null or empty, 
	 * friends are null or acquaintances are null.
	 * 
	 */
	public Person(String name, ArrayList<Person> friends, ArrayList<Person> acquaintances) throws IllegalArgumentException {
		if(name == null)throw new IllegalArgumentException("Null name is an invalid name");
		if(name.equals(""))throw new IllegalArgumentException("Empty name is an invalid name");
		if(friends == null)throw new IllegalArgumentException("Null friends");
		if(acquaintances==null)throw new IllegalArgumentException("Null acquaintances");
		this.name = name;
		this.friends = friends;
		this.acquaintances=acquaintances;
	}

	/**
	 * Inserts the {@link Person} into the specified {@link FriendQueue} with the information of the numberOfFriends to be sneaked
	 * @param friendQueue to be added 
	 * @param numberOfFriendsToSneak to sneak 
	 * @throws IllegalArgumentException if the friendQueue is null, numberOfFriends is smaller 
	 * than 0 or bigger than 10 
	 */
	public void enterIntoQueue(FriendQueue friendQueue, int numberOfFriendsToSneak)throws IllegalArgumentException {
		if(numberOfFriendsToSneak <MIN)throw new IllegalArgumentException("Number of friends must be bigger than 0 ");
		if(numberOfFriendsToSneak >MAX)throw new IllegalArgumentException("Number of friends must be smaller than 11");
		if(friendQueue ==null)throw new IllegalArgumentException("Null friendQueue value");
		totalFriendsToSneak = numberOfFriendsToSneak;
		friendsSneaked = MIN;
		friendQueue.addPerson(this);
		
	}
	
	/**
	 * Sneaks the {@link Person} into the specified {@link FriendQueue}
	 * If this person has more than one friend who can sneak him, it choose the friend in the best position
	 * @param friendQueue to be sneaked
	 * @throws IllegalArgumentException if the friendQueue is null,
	 * the {@link Person} has not any friends in the specified friendQueue or the friends who are in the queue are not allowed to sneak more friends.
	 */
	public void sneakIntoQueue(FriendQueue friendQueue) throws IllegalArgumentException {
		if(friendQueue==null)throw new IllegalArgumentException("Null friendQueue value");
		if(friendQueue.contains(this))throw new IllegalArgumentException("This Person is already in the queue");
		PriorityQueue<Integer> friendsPosition = new PriorityQueue<Integer>();
		Person friend;
		int position;
		for(int i = 0;i<friends.size();i++){
			friend =friends.get(i);
			if(friendQueue.contains(friend)&friend.getNumberOfFriendsToSneak(friendQueue)>MIN&friend.isAFriend(this)){
				position = friendQueue.getPosition(friend);
				friendsPosition.add(position);
			}
		}
		if(friendsPosition.size()==0)
			throw new IllegalArgumentException("This person can not be sneaked in the given friendQueue");
		position = friendsPosition.poll();
		totalFriendsToSneak = MIN;
		friendQueue.getPerson(position).sneakAFriend();
		friendQueue.addPerson(this, position);
	}

	/**
	 * Increments the number of sneaked friends of a {@link Person}
	 */
	private void sneakAFriend() {
		friendsSneaked++;
		
	}

	/**
	 * Returns the total number of friends which the {@link Person} is able to sneak
	 * @param friendQueue where the {@link Person} is
	 * @return the number of friends which the {@link Person} is able to sneak 
	 * into the specified friendQueue
	 * @throws IllegalArgumentException if the friendQueue is null or the {@link Person}
	 * is not in the specified friendQueue
	 */
	public int getTotalNumberOfFriendsToSneak(FriendQueue friendQueue) throws IllegalArgumentException {
		checkFriendQueue(friendQueue);
		return totalFriendsToSneak;
	}

	/**
	 * Returns the number of friends which the {@link Person} has not sneaked yet
	 * @param friendQueue where the {@link Person} is
	 * @return the number of friends which the {@link Person} has not sneaked yet 
	 * @throws IllegalArgumentException if the frienQueue is null or the {@link Person}
	 * is not in the specified friendQueue
	 */
	public int getNumberOfFriendsToSneak(FriendQueue friendQueue) throws IllegalArgumentException {
		checkFriendQueue(friendQueue);
		return totalFriendsToSneak-friendsSneaked;
	}

	/**
	 * Checks FriendQueue reference
	 * @param friendQueue to check
	 * @throws IllegalArgumentException if the friendQueue is null or the specified person is not in the given frienQueue
	 */
	private void checkFriendQueue(FriendQueue friendQueue) throws IllegalArgumentException {
		if(friendQueue ==null)throw new IllegalArgumentException("Null friendQueue value");
		if(!friendQueue.contains(this))throw new IllegalArgumentException("This person is not in the given frienQueue");
	}


	/**
	 * Returns friends which the {@link Person} has sneaked
	 * @param friendQueue where the {@link Person} is
	 * @return friends which the {@link Person} has sneaked into the specified friendQueue 
	 * or null if the {@link Person} has not sneaked yet or there are not any friends to attend in the specified frienQueue 
	 * @throws IllegalArgumentException if the frienQueue is null or the {@link Person}
	 * is not in the specified friendQueue
	 */
	public Person[] getSneakFriends(FriendQueue friendQueue) throws IllegalArgumentException {
		checkFriendQueue(friendQueue);
		if(friendsSneaked==MIN)return null;
		int position = friendQueue.getPosition(this);
		if(position==1)return null;
		Person[] sneakedFriends = new Person[Math.min(position-1,friendsSneaked)];
		for(int i = 0;i<sneakedFriends.length;i++){
			sneakedFriends[i] = friendQueue.getPerson(position-1-i);
		}
		return sneakedFriends;
		
		
	}

	/**
	 * Return true if the specified person is a friend of the {@link Person} 
	 * @param person whose friendship with this {@link Person} is to be tested
	 * @return true if the specified person is a friend of the {@link Person}, false otherwise
	 * @throws IllegalArgumentException is the person is null
	 */
	public boolean isAFriend(Person person)throws IllegalArgumentException {
		if(person == null)throw new IllegalArgumentException("Null person value");
		return friends.contains(person);
	}

	/**
	 * Returns the friends of this {@link Person}
	 * @return the friends of this {@link Person} or null if there are none
	 */
	public Person[] getFriends() {
		int numberOfFriends = friends.size();
		if(numberOfFriends==0) return null;
		Person[] friend = new Person[numberOfFriends];
		return friends.toArray(friend);
	}
	
	/**
	 * Transforms a strange person into a acquaintance of this {@link Person}
	 * The operation is reciprocal, when the person A meets person B, the person B meets person A
	 * @param person to meet with
	 * @throws IllegalArgumentException if the specified person is already an acquaintance,
	 * a friend or is null
	 */
	public void meet(Person person) throws IllegalArgumentException {
		if(person==null)throw new IllegalArgumentException("Null person value");
		if(friends.contains(person))throw new IllegalArgumentException("Can not meet a person who is already a friend");
		if(acquaintances.contains(person))throw new IllegalArgumentException("Can not meet a person who is already an acquaintance");
		acquaintances.add(person);
		if(person.isAnAcquaintance(this))return;
		person.meet(this);
	}


	/**
	 * Return true if the specified person is an acquaintance of the {@link Person} 
	 * @param person whose friendship with this {@link Person} is to be tested
	 * @return true if the specified person is an acquaintance of the {@link Person}, false otherwise
	 * @throws IllegalArgumentException is the person is null
	 */
	public boolean isAnAcquaintance(Person person) throws IllegalArgumentException {
		if(person==null)throw new IllegalArgumentException("Null person value");
		return acquaintances.contains(person);
	}


	/**
	 * Transforms an acquaintance person into a friend of this {@link Person}
	 * @param person to meet with
	 * @throws IllegalArgumentException if the specified person is already a friend,
	 * a strange or is null
	 */
	public void makeAFriend(Person person) {
		if(person==null)throw new IllegalArgumentException("Null person value");
		if(!acquaintances.contains(person))throw new IllegalArgumentException("The given person must be an acquaintance to be a friend");
		acquaintances.remove(person);
		friends.add(person);
		
	}


	/**
	 * Transforms a friend person into an acquaintance of this {@link Person}
	 * @param person to broke with
	 * @throws IllegalArgumentException if the specified person is already an acquaintance,
	 * a strange or is null
	 */
	public void loseAFriend(Person person) {
		if(person==null)throw new IllegalArgumentException("Null person value");
		if(!friends.contains(person))throw new IllegalArgumentException("The given person must be a friend of this person");
		friends.remove(person);
		acquaintances.add(person);
		
	}

}
