package tds.uva.es.albegut;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FriendQueueSequenceTest.class, TestFriendQueue.class, TestIsolatedFriendQueue.class, TestPerson.class })
public class AllTests {

}
