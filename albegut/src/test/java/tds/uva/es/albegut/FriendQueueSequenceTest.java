package tds.uva.es.albegut;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class FriendQueueSequenceTest {

	@Test
	public void sequenceTest() {
		
		ArrayList<Person> friends = new ArrayList<Person>();
		ArrayList<Person> acquaintances = new ArrayList<Person>();
		Person person = new Person("Kate",friends,acquaintances);
		
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Jonh",friends2,acquaintances2);
		
		friends.add(person2);
		friends2.add(person);
		
		
		//Initial state: empty queue
		FriendQueue queue = new FriendQueue();
		
		//Intermediate State: queue with people
		person.enterIntoQueue(queue, 1);
		assertEquals(person, queue.nextPersonToAttend());
		
		person2.sneakIntoQueue(queue);
		assertEquals(person2, queue.nextPersonToAttend());
		
		queue.attendNextPerson();
		assertEquals(person, queue.nextPersonToAttend());
		
		//Back to initial state
		queue.attendNextPerson();
		
	}

}
