package tds.uva.es.albegut;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


public class TestFriendQueue {
	
	
	private FriendQueue queue;
	private ArrayList<Person> friends;
	private ArrayList<Person> acquaintances;
	private Person person;
	
	@Before
	public void setUp(){
		queue = new FriendQueue();
		friends = new ArrayList<Person>();
		acquaintances = new ArrayList<Person>();
		person = new Person("Jonh",friends,acquaintances);

	}

	
	/*
	 * See next person to attend :valid cases 	
	 */
	
	@Test
	public void nextPersonToAttend(){
		queue.addPerson(person);
		assertEquals(person,queue.nextPersonToAttend());
	}
	
	/*
	 * See next person to attend : invalid cases
	 */
	
	@Test(expected=IllegalStateException.class)
	public void nextPersonToAttendWithEmptyQueue(){
		queue.nextPersonToAttend();
	}
	
	/*
	 * Attend next person: valid cases
	 */
	
	@Test
	public void attendNextPerson(){
		queue.addPerson(person);
		assertEquals(person, queue.attendNextPerson());
		assertFalse(queue.contains(person));
	}
	
	/*
	 * Attend next person: invalid cases
	 */
	
	@Test(expected=IllegalStateException.class)
	public void attendPersonWithEmptyQueue(){
		queue.attendNextPerson();
	}
	
	/*
	 * Add new person into the queue: valid cases  
	 */
	
	@Test
	public void addNewPersonIntoTheQueue(){
		queue.addPerson(person);
		assertTrue(queue.contains(person));
	}

	/*
	 * Add new person into the queue: invalid cases  
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void addPersonWhoAreInTheQueue(){
		queue.addPerson(person);
		queue.addPerson(person);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addNullPerson(){
		queue.addPerson(null);
	}
	
	/*
	 * Get position: valid cases
	 */
	
	@Test
	public void getPositionValidPerson(){
		queue.addPerson(person);
		assertEquals(1, queue.getPosition(person));
	}
	
	/*
	 * Get position: invalid cases
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void getPositionNullPerson(){
		queue.getPosition(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getPositionOfAPersonWhoIsNotInTheQueue(){
		queue.getPosition(person);
	}
	
	
	
	
}
