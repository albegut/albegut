package tds.uva.es.albegut;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;



public class TestIsolatedFriendQueue{
	
	@Mock
	private Person person;
	
	
	private FriendQueue queue;
	
	
	@Before
	public void setUp(){
		queue = new FriendQueue();
		person = createMock(Person.class);
		

	}

	
	/*
	 * See next person to attend :valid cases 	
	 */
	
	@Test
	public void nextPersonToAttend(){
		replay(person);
		queue.addPerson(person);
		assertEquals(person,queue.nextPersonToAttend());
		verify(person);
	}
	
	/*
	 * See next person to attend : invalid cases
	 */
	
	@Test(expected=IllegalStateException.class)
	public void nextPersonToAttendWithEmptyQueue(){
		replay(person);
		queue.nextPersonToAttend();
		verify(person);
	}
	
	/*
	 * Attend next person: valid cases
	 */
	
	@Test
	public void attendNextPerson(){
		replay(person);
		queue.addPerson(person);
		assertEquals(person, queue.attendNextPerson());
		assertFalse(queue.contains(person));
		verify(person);
	}
	
	/*
	 * Attend next person: invalid cases
	 */
	
	@Test(expected=IllegalStateException.class)
	public void attendPersonWithEmptyQueue(){
		replay(person);
		queue.attendNextPerson();
		verify(person);
	}
	
	/*
	 * Add new person into the queue: valid cases  
	 */
	
	@Test
	public void addNewPersonIntoTheQueue(){
		replay(person);
		queue.addPerson(person);
		assertTrue(queue.contains(person));
		verify(person);
	}

	/*
	 * Add new person into the queue: invalid cases  
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void addPersonWhoAreInTheQueue(){
		replay(person);
		queue.addPerson(person);
		queue.addPerson(person);
		verify(person);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addNullPerson(){
		queue.addPerson(null);
	}
	
	/*
	 * Get position: valid cases
	 */
	
	@Test
	public void getPositionValidPerson(){
		replay(person);
		queue.addPerson(person);
		assertEquals(1, queue.getPosition(person));
		verify(person);
	}
	
	/*
	 * Get position: invalid cases
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void getPositionNullPerson(){
		replay(person);
		queue.getPosition(null);
		verify(person);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getPositionOfAPersonWhoIsNotInTheQueue(){
		replay(person);
		queue.getPosition(person);
		verify(person);
	}
	
	
	
}
