package tds.uva.es.albegut;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class TestPerson {
	
	private FriendQueue queue;
	private ArrayList<Person> friends;
	private ArrayList<Person> acquaintances;
	private Person person;
	
	
	
	@Before
	public void setUp(){
		queue = new FriendQueue();
		friends = new ArrayList<Person>();
		acquaintances = new ArrayList<Person>();
		person = new Person("Jonh",friends,acquaintances);
		
	}
	
	/*
	 * Create a Person: valid cases
	 */
	
	@Test
	public void newPersonWithFriendsAndAcquaintances(){
		ArrayList<Person> friends = new ArrayList<Person>();
		ArrayList<Person> acquaintances = new ArrayList<Person>();
		Person person = new Person("Jonh",friends,acquaintances);
	}
	
	
	/*
	 * Create a Person: invalid cases
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void newPersonWithEmpyName(){
		ArrayList<Person> friends = new ArrayList<Person>();
		ArrayList<Person> acquaintances = new ArrayList<Person>();
		person = new Person("",friends,acquaintances);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void newPersonWithNullName(){
		ArrayList<Person> friends = new ArrayList<Person>();
		ArrayList<Person> acquaintances = new ArrayList<Person>();
		person = new Person(null,friends,acquaintances);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void newPersonWithNullFriends(){
		ArrayList<Person> friends = null;
		ArrayList<Person> acquaintances = new ArrayList<Person>();
		person = new Person("Jonh",friends,acquaintances);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void newPersonWithNullAcquaintances(){
		ArrayList<Person> friends = new ArrayList<Person>();
		ArrayList<Person> acquaintances = null;
		person = new Person("Jonh",friends,acquaintances);
	}
	
	/*
	 *Enter into the queue : valid cases
	 */
	
	@Test
	public void enterIntoQueueWithoutSneakFriends(){
		person.enterIntoQueue(queue,0);
		assertTrue(queue.contains(person));
		assertEquals(1,queue.getPosition(person));
	}
	
	@Test
	public void enterIntoQueueSneakingFriends(){
		person.enterIntoQueue(queue,10);
		assertTrue(queue.contains(person));
		assertEquals(1,queue.getPosition(person));
	}
	
	/*
	 * Enter into the queue: invalid cases 
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void enterIntoNullQueue(){
		FriendQueue queue = null;
		person.enterIntoQueue(queue,10);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void enterIntoQueueWithNoMinimumFriends(){
		person.enterIntoQueue(queue,-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void enterIntoQueueExceedingTheLimit(){
		person.enterIntoQueue(queue,11);
	}
	
	
	
	/*
	 * Sneak into the queue: valid cases
	 */
	
	@Test
	public void sneakIntoQueue(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		friends2.add(person);
		friends.add(person2);
		person2.enterIntoQueue(queue, 1);
		person.sneakIntoQueue(queue);
		assertTrue(queue.contains(person));
		assertEquals(1,queue.getPosition(person));
		assertEquals(2,queue.getPosition(person2));
	}
	
	/*
	 * Sneak into the queue: invalid cases 
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void sneakIntoNullQueue(){
		FriendQueue queue = null;
		person.sneakIntoQueue(queue);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void sneakIntoQueueWithoutFriends(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.enterIntoQueue(queue, 1);
		person.sneakIntoQueue(queue);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void sneakIntoQueueWithFriendsNotAllowedToSneakMore(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		friends2.add(person);
		friends.add(person2);
		person2.enterIntoQueue(queue,0);
		person.sneakIntoQueue(queue);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void sneakIntoQueueWithFriendWhoDoesNotConsiderYouAFriend(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		friends.add(person2);
		person2.enterIntoQueue(queue,1);
		person.sneakIntoQueue(queue);
	}
	
	@Test(expected =IllegalArgumentException.class)
	public void sneakIntoQueueWichContainsThePerson(){
		person.enterIntoQueue(queue, 0);
		person.sneakIntoQueue(queue);
	}
	
	/*
	 *Get total number of friends to sneak: valid cases
	 */
	
	@Test
	public void totalNumberOfFriendsToSneakValidQueue(){
		person.enterIntoQueue(queue, 10);
		assertEquals(10, person.getTotalNumberOfFriendsToSneak(queue));
	}
	
	
	/*
	 *Get total number of friends to sneak: invalid cases
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void totalNumberOfFriendsToSneakInNullQueue(){
		person.enterIntoQueue(queue, 10);
		person.getTotalNumberOfFriendsToSneak(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void totalNumberOfFriendsToSneakIntoInvalidQueue(){
		person.enterIntoQueue(queue, 10);
		FriendQueue queue2 = new FriendQueue();
		person.getTotalNumberOfFriendsToSneak(queue2);
	}
	
	/*
	 * Get number of friends to sneak: valid cases
	 */
	
	@Test
	public void numberOfFriendsToSneakSneakingSomeone(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		friends2.add(person);
		friends.add(person2);
		person2.enterIntoQueue(queue, 2);
		person.sneakIntoQueue(queue);
		assertEquals(1, person2.getNumberOfFriendsToSneak(queue));
	}
	
	@Test 
	public void numberOfFriendsToSneakWithoutSneaking(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		friends2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.enterIntoQueue(queue, 2);
		assertEquals(2,person2.getNumberOfFriendsToSneak(queue));
	}
	
	/*
	 * Get number of friends to sneak: invalid cases
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void restOfFriendsToSneakInNullQueue(){
		person.enterIntoQueue(queue, 10);
		person.getNumberOfFriendsToSneak(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void restOfFriendsToSneakIntoInvalidQueue(){
		person.enterIntoQueue(queue, 10);
		FriendQueue queue2 = new FriendQueue();
		person.getNumberOfFriendsToSneak(queue2);
	}
	
	/*
	 * Get sneak friends: valid cases
	 */
	
	@Test
	public void getSneakFriendsIntoTheQueueWithoutSneaking(){
		person.enterIntoQueue(queue, 5);
		assertNull( person.getSneakFriends(queue));
	}
	
	@Test
	public void getSneakFriendsIntoTheQueueWithoutFriendsToAttend(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		friends2.add(person);
		friends.add(person2);
		person2.enterIntoQueue(queue, 1);
		person.sneakIntoQueue(queue);
		queue.attendNextPerson();
		assertNull(person2.getSneakFriends(queue));
	}
	
	@Test
	public void getSneakFriendsIntoTheQueueWithFriendsToAttend(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		friends2.add(person);
		friends.add(person2);
		person2.enterIntoQueue(queue, 1);
		person.sneakIntoQueue(queue);
		Person[] sneakFriends = new Person[]{person};
		assertArrayEquals(sneakFriends, person2.getSneakFriends(queue));
	}
	
	/*
	 * Get sneak friends: invalid cases 	
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void getSneakFriendsInNullQueue(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		friends2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.enterIntoQueue(queue, 1);
		person.sneakIntoQueue(queue);
		person2.getSneakFriends(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getSneakFriendsIntoInvalidQueue(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		friends2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.enterIntoQueue(queue, 1);
		person.sneakIntoQueue(queue);
		FriendQueue queue2 = new FriendQueue();
		person2.getSneakFriends(queue2);
	}
	
	
	/*
	 * Are friends: valid cases
	 */
	
	@Test
	public void areFriendsWithValidFriend(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		friends2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		assertTrue(person2.isAFriend(person));
	}

	@Test
	public void areFriendsWithAnAcquaintance(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		acquaintances2.add(person);
		Person person2 =  new Person("Kate",friends2,acquaintances2);
		assertFalse(person2.isAFriend(person));
	}
	
	@Test
	public void areFriendsWithAStrange(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 =  new Person("Kate",friends2,acquaintances2);
		assertFalse(person2.isAFriend(person));
	}
	
	/*
	 * Are friends: invalid cases
	 */
	
	@Test(expected = IllegalArgumentException.class) 
	public void areFriendsWithNullPerson(){
		person.isAFriend(null);
	}
	
	/*
	 * Are Acquaintances: valid cases
	 */
	
	@Test
	public void areAcquaintancesWithAnAcquaintance(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		acquaintances2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		assertTrue(person2.isAnAcquaintance(person));
	}

	@Test
	public void areAcquaintancesWithAFriend(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		friends2.add(person);
		Person person2 =  new Person("Kate",friends2,acquaintances2);
		assertFalse(person2.isAnAcquaintance(person));
	}
	
	@Test
	public void areAcquaintancesWithAStrange(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 =  new Person("Kate",friends2,acquaintances2);
		assertFalse(person2.isAnAcquaintance(person));
	}
	
	
	/*
	 * Are Acquaintances: invalid cases
	 */
	@Test(expected = IllegalArgumentException.class) 
	public void areAnAcquaintanceWithNullPerson(){
		person.isAnAcquaintance(null);
	}
	
	/*
	 * Get Friends: valid cases
	 */
	
	@Test 
	public void getFriendsWithNoFriends(){
		assertNull(person.getFriends());
	}
	
	@Test 
	public void getFriends(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		friends2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		assertArrayEquals(friends2.toArray(), person2.getFriends());
	}
	
	/*
	 * Meet someone: valid cases
	 */
	
	@Test 
	public void meetSomeoneStrange(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.meet(person);
		assertTrue(person2.isAnAcquaintance(person));
		assertTrue(person.isAnAcquaintance(person2));
	}
	
	/*
	 * Meet someone: invalid cases
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void meetNone(){
		person.meet(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void meetAnAcquaintance(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		acquaintances2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.meet(person);
	
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void meetAfriend(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		friends2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.meet(person);
	
	}
	
	
	/*
	 * Make a friend: valid cases
	 */
	
	@Test
	public void makeAFriendFromAnAcquaintance(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		acquaintances2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.makeAFriend(person);
		assertTrue(person2.isAFriend(person));
		
	}
	
	/*
	 * Make a friend: invalid cases
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void makeAFriendFromSomeoneStrange(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.makeAFriend(person);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void makeAFriendFromAFriend(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		friends2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.makeAFriend(person);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void makeAFriendFromNone(){
		person.makeAFriend(null);
	}
	
	
	/*
	 * Lose a friend: valid cases
	 */
	
	@Test
	public void loseAFriend(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		friends2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.loseAFriend(person);
		assertFalse(person2.isAFriend(person));
		assertTrue(person2.isAnAcquaintance(person));
		
	}
	
	/*
	 * Lose a friend: invalid cases
	 */
	
	@Test(expected = IllegalArgumentException.class)
	public void loseNone(){
		person.loseAFriend(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void loseAnAcquaintance(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		acquaintances2.add(person);
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.loseAFriend(person);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void loseAStrange(){
		ArrayList<Person> friends2 = new ArrayList<Person>();
		ArrayList<Person> acquaintances2 = new ArrayList<Person>();
		Person person2 = new Person("Kate",friends2,acquaintances2);
		person2.loseAFriend(person);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
